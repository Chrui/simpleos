<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	final String permission_tree = "/simpleos/module/backend/manager/permission_tree.jsp";
	final String permission_list = "/simpleos/module/backend/manager/permission_list.jsp";
%>
<jsp:include page="/simpleos/include/lc1.jsp" flush="true">
	<jsp:param value="<%=permission_tree%>" name="left" />
	<jsp:param value="<%=permission_list%>" name="center" />
</jsp:include>
