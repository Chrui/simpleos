package net.simpleos.backend.permission;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.core.id.ID;
import net.simpleframework.web.page.IForward;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.base.ajaxrequest.AbstractAjaxRequestHandle;
import net.simpleos.SimpleosUtil;
import net.simpleos.utils.StringsUtils;

/**
 * 
 * @author 李岩飞 
 * @email eliyanfei@126.com
 * @date 2014年11月29日 下午10:06:51 
 *
 */
public class PermissionAction extends AbstractAjaxRequestHandle {

	public IForward bindMenuWithJob(final ComponentParameter compParameter) throws Exception {
		final HttpServletRequest request = compParameter.request;
		return jsonForward(compParameter, new JsonCallback() {

			@Override
			public void doAction(final Map<String, Object> json) throws Exception {
				final String menuNames = request.getParameter("menuNames");
				final ITableEntityManager tMgr = SimpleosUtil.getTableEntityManager(SimpleosUtil.applicationModule, PermissionBean.class);
				final String[] checkedMenuArray = menuNames.split(",");
				try {
					tMgr.delete(new ExpressionValue(" job_id = ? ", new Object[] { request.getParameter("jobId") }));
				} catch (final Exception e) {
				}
				for (final String menuName : checkedMenuArray) {
					if (!StringsUtils.isNotBlank1(menuName)) {
						continue;
					}
					final PermissionBean permissionBean = new PermissionBean();
					permissionBean.setJob_id(ID.Utils.newID(request.getParameter("jobId")));
					permissionBean.setJob_name(request.getParameter("jobName"));
					permissionBean.setMenu_name(menuName);
					tMgr.insert(permissionBean);
				}
				PlatformUtis.DEFAULT_MENUS.clear();
				PlatformUtis.hasPermission = SimpleosUtil.getTableEntityManager(SimpleosUtil.applicationModule, PermissionBean.class).getCount(
						new ExpressionValue("1=1")) > 0;
			}

		});
	}
}
