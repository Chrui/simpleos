package net.simpleos.backend.permission;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author 李岩飞 
 * @email eliyanfei@126.com
 * @date 2014年11月29日 下午10:07:21 
 *
 */
public class PermissionSession {
	public Map<String, String> menuMap;
	private long createDate;

	public PermissionSession() {
		super();
		this.createDate = System.currentTimeMillis();
		menuMap = new HashMap<String, String>();
	}

	public long getCreateDate() {
		return createDate;
	}
}
