<h>simpleos</h>
========
<p>这是一个web开发框架，里面内置资讯，博客，论坛，文档以及用户角色组织架构等功能模块，开发简单，可以直接部署使用。</p>

<p>部分图例</p>
![输入图片说明](http://git.oschina.net/uploads/images/2015/0920/091129_4d6c9f6d_926.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2015/0920/091743_f39ce1e4_926.jpeg "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2015/0920/091152_ca262a2f_926.png "在这里输入图片标题")